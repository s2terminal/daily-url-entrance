import React from 'react';
import Layout from "../components/layout"
import EveryURL from "@s2terminal/every-url";

const NiftyPage = () => (
  <Layout>
    <EveryURL
      uniqueStorageKey="NiftyNewsSettings"
      urlSettings={
        {
          host: {
            name: "HOST",
            type: "static",
            default: "https://news.nifty.com/"
          },
          beforeDatePath: {
            name: "BEFORE DATE PATH",
            type: "static",
            default: "topics/daily/"
          },
          datePath: {
            name: "DATE",
            type: "date",
            default: "YYYYMMDD"
          },
          afterDatePath: {
            name: "AFTER DATE PATH",
            type: "static",
            default: "/"
          }
        }
      }
    />
  </Layout>
);

export default NiftyPage
