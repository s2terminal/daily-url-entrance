import React from 'react';
import Layout from "../components/layout"
import EveryURL from "@s2terminal/every-url";

const YahooPage = () => (
  <Layout>
    <EveryURL
      uniqueStorageKey="YahooNewsSettings"
      urlSettings={
        {
          host: {
            name: "ホスト名",
            type: "static",
            default: "https://news.yahoo.co.jp/"
          },
          beforeDatePath: {
            name: "パス",
            type: "static",
            default: "list/?d="
          },
          datePath: {
            name: "日付",
            type: "date",
            default: "YYYYMMDD"
          }
        }
      }
      settings={
        {
          saved: "設定を保存しました！",
          otherFeature: "詳細",
          autoRedirect: "自動リダイレクト",
          autoRedirectDescription: "下記URLをブックマークすると、自動でリダイレクトします",
          mainURL: "今日のYahoo!ニュース",
          pathSettings: "設定変更"
        }
      }
    />
  </Layout>
);

export default YahooPage
