import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
    <h2>これは何</h2>
    <p>日々変わるURLをブックマークで飛べるようにします。</p>
    <ul>
      <li><Link to="/yahoo/">Yahoo!ニュース</Link></li>
      <li><Link to="/nifty/">Nifty News</Link></li>
    </ul>
  </Layout>
)

export default IndexPage
