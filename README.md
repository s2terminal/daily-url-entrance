# Daily URL Entrance

## これは何
https://news.yahoo.co.jp/list/?d=20190414 や  https://news.nifty.com/topics/daily/20190412/ のような、日々変わるURLをブックマークで飛べるようにします。

拙作 React Component [@s2terminal/every\-url](https://www.npmjs.com/package/@s2terminal/every-url) <a href="https://badge.fury.io/js/%40s2terminal%2Fevery-url"><img src="https://badge.fury.io/js/%40s2terminal%2Fevery-url.svg" alt="npm version" height="18"></a> の使用例です。

## 使ってみる
https://s2terminal.gitlab.io/daily-url-entrance/ にアクセス。

## 開発
```
$ npx gastby develop
```
open http://localhost:8080

## ライセンス
[MIT](LICENSE).
